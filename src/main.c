#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "mem.h"
#include "mem_internals.h"
#include "tests.h"

int main(void){
   int res = 0;
   res += test_1();
   if (test_1()){
       printf("1 Тест пройден /n");
   }
   else{
       printf("1 Тест не пройден /n");
   }
   res += test_2();
   if (test_2()){
       printf("2 Тест пройден /n");
   }
   else{
       printf("2 Тест не пройден /n");
   }
   res += test_3();
   if (test_3()){
       printf("3 Тест пройден /n");
   }
   else{
       printf("3 Тест не пройден /n");
   }
   res += test_4();
   if (test_4()){
       printf("4 Тест пройден /n");
   }
   else{
       printf("4 Тест не пройден /n");
   }
   res += test_5();
   if (test_5()){
       printf("5 Тест пройден /n");
   }
   else{
       printf("5 Тест не пройден /n");
   }
    if (res == 5){
        printf("Все тесты пройдены /n");
    }
}
