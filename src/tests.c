#define _DEFAULT_SOURCE
#define HEAP_SIZE 1000

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "mem.h"
#include "mem_internals.h"
#include "tests.h"

/* Обычное успешное выделение памяти */
bool test_1(){

    void* heap = heap_init(HEAP_SIZE);
    if(!heap){
        return false;
    }

    debug_heap(stdout, heap);
    void* alloc = _malloc(10);

    if(!alloc){
        return false;
    }

    debug_heap(stdout, heap);
    _free(alloc);

    munmap(HEAP_START, 8192);

    return true;
}

/* Освобождение одного блока из нескольких выделенных */
bool test_2(){
    
    void* heap = heap_init(HEAP_SIZE);

    if(!heap){
        return false;
    }

    debug_heap(stdout, heap);

    void* alloc_1 = _malloc(10);
    void* alloc_2 = _malloc(20);

    if(!alloc_1 || !alloc_2){
        return false;
    }

    debug_heap(stdout, heap);
    _free(alloc_1);
    debug_heap(stdout, heap);

    munmap(HEAP_START, 8192);
    return true;
}

// Освобождение двух блоков из нескольких выделенных
bool test_3(){

    void* heap = heap_init(HEAP_SIZE);

    if(!heap){
        return false;
    }

    debug_heap(stdout, heap);

    void* alloc_1 = _malloc(10);
    void* alloc_2 = _malloc(20);
    void* alloc_3 = _malloc(30);

    if(!alloc_1 || !alloc_2 || !alloc_3){
        return false;
    }

    debug_heap(stdout, heap);
    _free(alloc_1);
    debug_heap(stdout, heap);
    _free(alloc_3);
    debug_heap(stdout, heap);

    munmap(HEAP_START, 8192);
    return true;
}

// Память закончилась, новый регион памяти расширяет старый
bool test_4(){

    void* heap = heap_init(HEAP_SIZE);

    debug_heap(stdout, heap);

    void* alloc_1 = _malloc(10000);

    if(!alloc_1){
        return false;
    }

    debug_heap(stdout, heap);
    _free(alloc_1);

    munmap(heap, 20480);
    return true;
}

// Память закончилась, старый регион памяти не расширить
bool test_5(){

    void* heap = heap_init(HEAP_SIZE);

    void* map = mmap(HEAP_START + 8192, 4096, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);

    debug_heap(stdout, heap);

    void* alloc_1 = _malloc(10000);

    if(!alloc_1){
        return false;
    }

    debug_heap(stdout, heap);

    _free(alloc_1);
    debug_heap(stdout, heap);

    munmap(map, 4096);
    return true;
}
